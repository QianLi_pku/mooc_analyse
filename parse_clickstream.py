#!/bin/python
## Here is the python script to convert clickstream json file to mysql relation database.
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import library to deal with json
import json
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

fileDir = '../data/'
# files = ['chemistry-001', 'chemistry-002', 'chemistry-003']
files = ['bioinfo-001', 'bioinfo-002']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent']

for file_name in files:
	connection = pymysql.connect(host='localhost',
							user='root',
							db=file_name,
							charset='utf8',
							cursorclass=pymysql.cursors.DictCursor)

	# First, create tables for clickstream
	try:
		with connection.cursor() as cursor:
			sql = ("DROP TABLE IF EXISTS `clickstream`;")
			cursor.execute(sql)
			connection.commit()
	finally:
		pass
	
	try:
		with connection.cursor() as cursor:
			sql = ("CREATE TABLE IF NOT EXISTS `clickstream` ( "
				"`click_id` INT NOT NULL AUTO_INCREMENT, "
				"`key` TEXT DEFAULT NULL, "
				"`value` TEXT DEFAULT NULL, "
				"`username` VARCHAR(128) DEFAULT NULL, "
				"`timestamp` BIGINT(20) DEFAULT NULL, "
				"`page_url` TEXT DEFAULT NULL, "
				"`language` TEXT DEFAULT NULL, "
				#"`from` TEXT DEFAULT NULL, "
				"`user_ip` VARCHAR(128) DEFAULT NULL, "
				"`user_agent` TEXT CHARACTER SET utf8 DEFAULT NULL, "
				"PRIMARY KEY (`click_id`));")
			cursor.execute(sql)
			connection.commit()
	finally:
		pass
	
	# Second, load data from files and insert into the database.
	testFile = open(fileDir + file_name + '/' +  file_name + '_clickstream_export')
	cnt = 0
	for eachLine in testFile:
		try:
			data = json.loads(eachLine)
		except:
			continue
		cnt += 1
		for entry in entries:
			if not data.has_key(entry):
				data[entry] = ""
		try:
			with connection.cursor() as cursor:
				# Create a new record
				sql = "INSERT INTO `clickstream` (`key`, `value`, `username`, `page_url`, `language`, `timestamp`, `user_ip`, `user_agent`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
				cursor.execute(sql, (data['key'], data['value'], data['username'], data['page_url'], data['language'], data['timestamp'], data['user_ip'], data['user_agent']))
			connection.commit()
		finally:
			print cnt
	connection.close()
	#print type(data['key'])
	testFile.close()
