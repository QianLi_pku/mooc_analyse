#!/bin/python
## generate user click sequence for daily model
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './daily_output/'
files = ['chemistry-003', 'bioinfo-002']

start_times = {'chemistry-003': '2015/2/1', 'bioinfo-002': '2014/1/28'}
durations = {'chemistry-003': 149, 'bioinfo-002': 88}

for file_name in files:
  users = [] # store all users who have grade > 0
  user_lecture = {}
  user_forum = {}
  user_quiz = {}
  user_other = {}
  user_grade = {}
  total_days = durations[file_name]

  inFile1 = open(fileDir + file_name + '_1D_forum.txt', 'r');
  inFile2 = open(fileDir + file_name + '_1D_lecture.txt', 'r')
  inFile3 = open(fileDir + file_name + '_1D_quiz.txt', 'r')
  inFile4 = open(fileDir + file_name + '_1D_all.txt', 'r')

  for eachLine in inFile1:
    words = eachLine.strip('\n').split('\t')
    if len(words) < 1:
      break
    user_id = words[0]
    if user_id not in users:
      users.append(user_id)
      user_forum[user_id] = [0 for i in xrange(total_days)]
      user_lecture[user_id] = [0 for i in xrange(total_days)]
      user_quiz[user_id] = [0 for i in xrange(total_days)]
      user_other[user_id] = [0 for i in xrange(total_days)]

    for i in xrange(1, len(words)):
      user_forum[user_id][i-1] = int(words[i])
  
  for eachLine in inFile2:
    words = eachLine.strip('\n').split('\t')
    if len(words) < 1:
      break
    user_id = words[0]

    for i in xrange(1, len(words)):
      user_lecture[user_id][i-1] = int(words[i])

  for eachLine in inFile3:
    words = eachLine.strip('\n').split('\t')
    if len(words) < 1:
      break
    user_id = words[0]

    for i in xrange(1, len(words)):
      user_quiz[user_id][i-1] = int(words[i])

  for eachLine in inFile4:
    words = eachLine.strip('\n').split('\t')
    if len(words) < 1:
      break
    user_id = words[0]
    
    for i in xrange(1, len(words)):
      user_other[user_id][i-1] = int(words[i])

  print "finish loading!...\n"

  for duration in xrange(1, total_days, 7):
    outFile1 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_forum.txt', 'w')
    outFile2 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_lecture.txt', 'w')
    outFile3 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_quiz.txt', 'w')
    outFile4 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_all.txt', 'w')

    for user in users:
      outFile1.write(str(user))
      outFile2.write(str(user))
      outFile3.write(str(user))
      outFile4.write(str(user))

      for i in xrange(0, duration):
        outFile1.write('\t' + str(user_forum[user][i]))
        outFile2.write('\t' + str(user_lecture[user][i]))
        outFile3.write('\t' + str(user_quiz[user][i]))
        outFile4.write('\t' + str(user_other[user][i]))
      outFile1.write('\n')
      outFile2.write('\n')
      outFile3.write('\n')
      outFile4.write('\n')
    outFile1.close()
    outFile2.close()
    outFile3.close()
    outFile4.close()

  for duration in xrange(total_days, total_days+1):
    outFile1 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_forum.txt', 'w')
    outFile2 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_lecture.txt', 'w')
    outFile3 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_quiz.txt', 'w')
    outFile4 = open(fileDir + file_name + '_' + str(duration) + 'day_1D_all.txt', 'w')

    for user in users:
      outFile1.write(str(user))
      outFile2.write(str(user))
      outFile3.write(str(user))
      outFile4.write(str(user))

      for i in xrange(0, duration):
        outFile1.write('\t' + str(user_forum[user][i]))
        outFile2.write('\t' + str(user_lecture[user][i]))
        outFile3.write('\t' + str(user_quiz[user][i]))
        outFile4.write('\t' + str(user_other[user][i]))
      outFile1.write('\n')
      outFile2.write('\n')
      outFile3.write('\n')
      outFile4.write('\n')
    outFile1.close()
    outFile2.close()
    outFile3.close()
    outFile4.close()

  inFile1.close()
  inFile2.close()
  inFile3.close()
  inFile4.close()
