#!/bin/python
## Here is the python script to convert clickstream json file to mysql relation database.
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import library to deal with json
import json
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors
import random

fileDir = './output/'
files = ['chemistry-001', 'chemistry-002', 'chemistry-003']
# files = ['bioinfo-001', 'bioinfo-002']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent']
time_sufixes = ['_1D', '_3D', '_1W', '_1M']
cata_sufixes = ['_forum', '_lecture', '_quiz', '_all']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent', 'id', 'session_user_id', 'normal_grade', 'distinction_grade', 'achievement_level', 'authenticated_overall', 'ace_grade', 'passed_ace']
durations = {'chemistry-001': 121, 'chemistry-002': 126, 'chemistry-003': 149}
# durations = {'chemistry-001': 121, 'chemistry-002': 121, 'chemistry-003': 121}
# durations = {'bioinfo-001': 146, 'bioinfo-002': 88}
ngrams = [2, 3, 4, 5]

for file_name in files:

  duration = durations[file_name]
  users_vector = {}
  cata_names = {}
  pass_cnt = 0
  fail_cnt = 0

  # First, read the users' scores
  input_file = open(fileDir + file_name + '_valid_grade.txt', 'r')
  for eachLine in input_file:
    words = eachLine.strip('\n').split('\t')
    if len(words) < 1:
      break
    user_id = words[0]
    if user_id not in users_vector:
      users_vector[user_id] = {}

    score = float(words[1])
    if (score >= 60.0):
      # if (score >= 85.0):
      #   users_vector[user_id]["score"] = "execellent"
      # else:
      #   users_vector[user_id]["score"] = "pass"
      users_vector[user_id]["score"] = "pass"
      pass_cnt += 1
    else:
      users_vector[user_id]["score"] = "fail"
      fail_cnt += 1

  input_file.close()

  # Second, read single 1D, 3D, 1W, 1M value
  for cata_sufix in cata_sufixes:
    for time_sufix in time_sufixes:
      input_file = open(fileDir + file_name + time_sufix + cata_sufix + '.txt', 'r')
      for eachLine in input_file:
        words = eachLine.strip('\n').split('\t')
        if len(words) < 1:
          break
        user_id = words[0]
        if user_id not in users_vector:
          continue

        for i in xrange(0, len(words) - 1):
          cata_name = '%d%s%s' % (i, time_sufix, cata_sufix)
          if cata_name not in cata_names:
            cata_names[cata_name] = 1
          users_vector[user_id][cata_name] = int(words[i+1])

      input_file.close()

      # Third, read 2, 3, 4, 5 n-gram files]
      for ngram in ngrams:
        total_catas = (1 << ngram) # 2^n number of [0, 0]...[1, 1]
        input_file = open(fileDir + file_name + time_sufix + cata_sufix + '_' + str(ngram) + 'gram.txt', 'r')

        for eachLine in input_file:
          words = eachLine.strip('\n').split('\t')
          if len(words) < 1:
            break
          user_id = words[0]
          if user_id not in users_vector:
            continue

          for i in xrange (0, len(words) - 1):
            tmpstr = '['
            for j in xrange(0, ngram):
              if ((i & (1 << (ngram - j - 1))) > 0):
                tmpstr += '1'
              else:
                tmpstr += '0'
            tmpstr += ']'
            cata_name = tmpstr + '%s%s' % (time_sufix, cata_sufix)
            if cata_name not in cata_names:
              cata_names[cata_name] = 1
            users_vector[user_id][cata_name] = int(words[i + 1])

        input_file.close()

  output_file = open(fileDir + file_name + "_unbalance_origin.csv", 'w')
  output_file.write("user_id")
  for cata_name in cata_names:
    output_file.write(","+cata_name)
  output_file.write(",score\n")

  for user_id in users_vector:
    user_info = users_vector[user_id]
    output_file.write(str(user_id))
    for cata_name in cata_names:
      output_file.write("," + str(user_info[cata_name]))
    output_file.write("," + str(user_info["score"]))
    output_file.write('\n')

  output_file.close()

