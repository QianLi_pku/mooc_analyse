#!/bin/python
## generate user click sequence for 1-day
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './output/'
files = ['chemistry-001', 'chemistry-002', 'chemistry-003']
#files = ['bioinfo-001', 'bioinfo-002']
time_sufixes = ['_1D', '_3D', '_1W', '1M']
cata_sufixes = ['_forum', 'lecture', 'quiz']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent', 'id', 'session_user_id', 'normal_grade', 'distinction_grade', 'achievement_level', 'authenticated_overall', 'ace_grade', 'passed_ace']
start_times = {'chemistry-001': '2013/9/1', 'chemistry-002': '2014/2/10', 'chemistry-003': '2015/2/1'}
end_times = {'chemistry-001': '2013/12/31', 'chemistry-002': '2014/6/16', 'chemistry-003': '2015/6/30'}
#start_times = {'bioinfo-001': '2013/9/4', 'bioinfo-002': '2014/1/28'}
#end_times = {'bioinfo-001': '2014/1/28', 'bioinfo-002': '2014/5/30'}
durations = {'chemistry-001': 121, 'chemistry-002': 126, 'chemistry-003': 149}
# durations = {'chemistry-001': 121, 'chemistry-002': 121, 'chemistry-003': 121}
# durations = {'bioinfo-001': 146, 'bioinfo-002': 88}

for file_name in files:
  users = [] # store all users who have grade > 0
  user_lecture = {}
  user_forum = {}
  user_quiz = {}
  user_other = {}
  user_grade = {}
  outFile1 = open(fileDir + file_name + '_1D_forum.txt', 'w')
  outFile2 = open(fileDir + file_name + '_1D_lecture.txt', 'w')
  outFile3 = open(fileDir + file_name + '_1D_quiz.txt', 'w')
  outFile4 = open(fileDir + file_name + '_1D_all.txt', 'w')

  connection = pymysql.connect(host='localhost',
              user='root',
              db=file_name,
              charset='utf8',
              cursorclass=pymysql.cursors.DictCursor)
  
  # First, select out all distinct users who have grades > 0
  cnt = 0
  try:
    with connection.cursor() as cursor:
      # Select 
      sql = "SELECT * FROM `course_grades` WHERE `normal_grade` > 0"
      cursor.execute(sql)
      result = cursor.fetchone()
      while (result):
        users.append(result['session_user_id'])
        user_lecture[result['session_user_id']] = [0 for i in xrange(maxlength)]
        user_forum[result['session_user_id']] = [0 for i in xrange(maxlength)]
        user_quiz[result['session_user_id']] = [0 for i in xrange(maxlength)]
        user_other[result['session_user_id']] = [0 for i in xrange(maxlength)]
        user_grade[result['session_user_id']] = 0
        cnt += 1
        result = cursor.fetchone()
  finally:
    print cnt
    print len(user_lecture)
    print len(user_forum)
    print len(user_quiz)

  # Second, select every user's own record from valid_users and output this user's sequence
  cnt = 0
  for user in users:
    try:
      with connection.cursor() as cursor:
        # Select this user's own record
        sql = "SELECT * FROM `valid_users` WHERE `username` = %s"
        cursor.execute(sql, user)
        result = cursor.fetchone()
        while (result):
          cnt += 1;
          timeframe = result['timestamp']/1000
          timediff = 0
          if user_grade[user] == 0:
            user_grade[user] = result['normal_grade']
          try:
            with connection.cursor() as cursor2:
              sql2 = "SELECT DATEDIFF(FROM_UNIXTIME(%s), %s) as timediff"
              start_time = start_times[file_name]
              cursor2.execute(sql2, (str(timeframe), start_time))
              result2 = cursor2.fetchone()
              timediff = result2['timediff']
          finally:
            activity = result['page_url']
            print activity
            if activity.find("lecture") != -1:
              user_lecture[user][timediff] = 1
            if activity.find("forum") != -1:
              user_forum[user][timediff] = 1
            if activity.find("quiz") != -1:
              user_quiz[user][timediff] = 1
            user_other[user][timediff] = 1 # at least he visit the site this day

          result = cursor.fetchone()
    finally:
        print(user_lecture[user])
        print(user_forum[user])
        print(user_quiz[user])
        print(user_other[user])
        print(user_grade[user])
        outFile1.write(str(user))
        outFile2.write(str(user))
        outFile3.write(str(user))
        outFile4.write(str(user))

        duration = durations[file_name]
        for i in xrange(0, duration):
          outFile1.write('\t' + str(user_forum[user][i]))
          outFile2.write('\t' + str(user_lecture[user][i]))
          outFile3.write('\t' + str(user_quiz[user][i]))
          outFile4.write('\t' + str(user_other[user][i]))
        outFile1.write('\n')
        outFile2.write('\n')
        outFile3.write('\n')
        outFile4.write('\n')
  print('real users: ' + str(cnt))
  outFile1.close()
  outFile2.close()
  outFile3.close()
  outFile4.close()
  connection.close()
