#!/bin/python
## daily data: generate user click sequence for 3-day, 1-week, 1-month
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './daily_output/'
files = ['chemistry-003', 'bioinfo-002']
time_sufixes = ['_3D', '_1W', '_1M']
cata_sufixes = ['_forum', '_lecture', '_quiz', '_all']

start_times = {'chemistry-003': '2015/2/1', 'bioinfo-002': '2014/1/28'}

durations = {'chemistry-003': 149, 'bioinfo-002': 88}

for file_name in files:
  total_days = durations[file_name]
  for duration in xrange(1, total_days, 7):
    for cata_sufix in cata_sufixes:
      input_file = open(fileDir + file_name + '_' + str(duration) + 'day_1D' + cata_sufix + '.txt', 'r')
      users_vector = {}
      for eachLine in input_file:
        words = eachLine.strip('\n').split('\t')
        if len(words) < 1:
          break
        user_id = words[0]
        if user_id not in users_vector:
          users_vector[user_id] = [0 for i in xrange(duration)]
        for i in xrange(1, len(words)):
          users_vector[user_id][i-1] = int(words[i])
        print(users_vector[user_id])
      # The interval between neighbor element
      interval = 1
      for time_sufix in time_sufixes:
        if time_sufix == '_3D':
          interval = 3
        if time_sufix == '_1W':
          interval = 7
        if time_sufix == '_1M':
          interval = 30
        output_file = open(fileDir + file_name + '_' + str(duration) + 'day' + time_sufix + cata_sufix + '.txt', 'w')
        for user in users_vector:
          output_file.write(str(user))
          #print str(user)
          day = 0
          while day < len(users_vector[user]):
            flag = 0
            for i in xrange(day, min(day + interval, len(users_vector[user]))):
              if users_vector[user][i] == 1:
                flag = 1
                break
            output_file.write('\t' + str(flag))
            #print '\t' + str(flag)
            day = day + interval
          output_file.write('\n')
          #print '\n'
        output_file.close()
      input_file.close()

  for duration in xrange(total_days, total_days+1):
    for cata_sufix in cata_sufixes:
      input_file = open(fileDir + file_name + '_' + str(duration) + 'day_1D' + cata_sufix + '.txt', 'r')
      users_vector = {}
      for eachLine in input_file:
        words = eachLine.strip('\n').split('\t')
        if len(words) < 1:
          break
        user_id = words[0]
        if user_id not in users_vector:
          users_vector[user_id] = [0 for i in xrange(duration)]
        for i in xrange(1, len(words)):
          users_vector[user_id][i-1] = int(words[i])
        print(users_vector[user_id])
      # The interval between neighbor element
      interval = 1
      for time_sufix in time_sufixes:
        if time_sufix == '_3D':
          interval = 3
        if time_sufix == '_1W':
          interval = 7
        if time_sufix == '_1M':
          interval = 30
        output_file = open(fileDir + file_name + '_' + str(duration) + 'day' + time_sufix + cata_sufix + '.txt', 'w')
        for user in users_vector:
          output_file.write(str(user))
          #print str(user)
          day = 0
          while day < len(users_vector[user]):
            flag = 0
            for i in xrange(day, min(day + interval, len(users_vector[user]))):
              if users_vector[user][i] == 1:
                flag = 1
                break
            output_file.write('\t' + str(flag))
            #print '\t' + str(flag)
            day = day + interval
          output_file.write('\n')
          #print '\n'
        output_file.close()
      input_file.close()
