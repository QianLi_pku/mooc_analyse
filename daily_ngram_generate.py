#!/bin/python
## generate 2, 3, 4, 5-gram for 1-day, 3-day, 1-week, 1-month for each user
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './daily_output/'
files = ['chemistry-003', 'bioinfo-002']

time_sufixes = ['_1D', '_3D', '_1W', '_1M']
cata_sufixes = ['_forum', '_lecture', '_quiz', '_all']

durations = {'chemistry-003': 149, 'bioinfo-002': 88}
ngrams = [2, 3, 4, 5]

for file_name in files:
  total_days = durations[file_name]
  for duration in xrange(1, total_days, 7):
    for cata_sufix in cata_sufixes:
      for time_sufix in time_sufixes:

        interval = 1
        if time_sufix == '_3D':
          interval = 3
        if time_sufix == '_1W':
          interval = 7
        if time_sufix == '_1M':
          interval = 30
        duration2 = duration / interval + 1
        input_file = open(fileDir + file_name + '_' + str(duration) + 'day' +time_sufix + cata_sufix + '.txt', 'r')
        users_vector = {}
        for eachLine in input_file:
          words = eachLine.strip('\n').split('\t')
          if len(words) < 1:
            break
          user_id = words[0]
          if user_id not in users_vector:
            users_vector[user_id] = [0 for i in xrange(duration2)]
          for i in xrange(1, len(words)):
            users_vector[user_id][i-1] = int(words[i])
          print(users_vector[user_id])

        for ngram in ngrams:
          total_catas = (1 << ngram) # 2^n number of [0, 0]...[1, 1]
          interval = ngram - 1
          output_file = open(fileDir + file_name + '_' + str(duration) + 'day' + time_sufix + cata_sufix + '_' + str(ngram) + 'gram.txt', 'w')
          for user in users_vector:
            ngram_seq = [0 for i in xrange(total_catas)]
            output_file.write(str(user))
            day = 0
            while day < (len(users_vector[user]) - interval):
              cnt = 0
              for i in xrange(day, day + interval + 1):
                cnt = (cnt << 1) + users_vector[user][i]
              ngram_seq[cnt] += 1
              day = day + 1

            for j in xrange(total_catas):
              output_file.write('\t' + str(ngram_seq[j]))

            output_file.write('\n')

          output_file.close()
        input_file.close()

  for duration in xrange(total_days, total_days+1):
    for cata_sufix in cata_sufixes:
      for time_sufix in time_sufixes:
        duration = durations[file_name]
        interval = 1
        if time_sufix == '_3D':
          interval = 3
        if time_sufix == '_1W':
          interval = 7
        if time_sufix == '_1M':
          interval = 30
        duration2 = duration / interval + 1
        input_file = open(fileDir + file_name + '_' + str(duration) + 'day' +time_sufix + cata_sufix + '.txt', 'r')
        users_vector = {}
        for eachLine in input_file:
          words = eachLine.strip('\n').split('\t')
          if len(words) < 1:
            break
          user_id = words[0]
          if user_id not in users_vector:
            users_vector[user_id] = [0 for i in xrange(duration2)]
          for i in xrange(1, len(words)):
            users_vector[user_id][i-1] = int(words[i])
          print(users_vector[user_id])

        for ngram in ngrams:
          total_catas = (1 << ngram) # 2^n number of [0, 0]...[1, 1]
          interval = ngram - 1
          output_file = open(fileDir + file_name + '_' + str(duration) + 'day' + time_sufix + cata_sufix + '_' + str(ngram) + 'gram.txt', 'w')
          for user in users_vector:
            ngram_seq = [0 for i in xrange(total_catas)]
            output_file.write(str(user))
            day = 0
            while day < (len(users_vector[user]) - interval):
              cnt = 0
              for i in xrange(day, day + interval + 1):
                cnt = (cnt << 1) + users_vector[user][i]
              ngram_seq[cnt] += 1
              day = day + 1

            for j in xrange(total_catas):
              output_file.write('\t' + str(ngram_seq[j]))

            output_file.write('\n')

          output_file.close()
        input_file.close()
    