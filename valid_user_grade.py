#!/bin/python
## generate user click sequence for 1-day, 3-day, 1-week, 1-month
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './output/'
files = ['chemistry-001', 'chemistry-002', 'chemistry-003']
# files = ['bioinfo-001', 'bioinfo-002']
time_sufixes = ['_1D', '_3D', '_1W', '1M']
cata_sufixes = ['_forum', 'lecture', 'quiz']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent', 'id', 'session_user_id', 'normal_grade', 'distinction_grade', 'achievement_level', 'authenticated_overall', 'ace_grade', 'passed_ace']

for file_name in files:
  users_grade = {} # store all users who have grade > 0
  
  outFile = open(fileDir + file_name + '_valid_grade.txt', 'w')

  connection = pymysql.connect(host='localhost',
              user='root',
              db=file_name,
              charset='utf8',
              cursorclass=pymysql.cursors.DictCursor)
  
  # First, select out all distinct users who have grades > 0
  cnt = 0
  try:
    with connection.cursor() as cursor:
      # Select 
      sql = "SELECT * FROM `course_grades` WHERE `normal_grade` > 0"
      cursor.execute(sql)
      result = cursor.fetchone()
      while (result):
        user_id = result['session_user_id']
        if user_id not in users_grade:
          users_grade[user_id] = result['normal_grade']
          cnt += 1
        result = cursor.fetchone()
  finally:
    print cnt
    print len(users_grade)
    pprint(users_grade)
    for user in users_grade:
      outFile.write(str(user) + '\t' + str(users_grade[user])  + '\n')

  outFile.close()
  connection.close()
