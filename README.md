# MOOC_analyse
Some useful code for analysing Coursera MOOC data, for my undergraduate research project.

### First, create database from coursera export
eg.
```
gunzip bioinfo*_SQL_*.gz
mysql -u root -e 'create database if not exists `bioinfo-002`'
find . -name 'bioinfo*002*.sql' | awk '{ print "source",$0 }' | mysql -u root bioinfo-002
```

### How to use it:
1. Use `parse_clickstream.py` to create a table in database
2. create a valid_user view, so that I could retrieve data quite easily.
```
SELECT * FROM `clickstream`, `course_grades` WHERE `clickstream`.`username` = `course_grades`.`session_user_id` and `normal_grade` > 0;
```

3. Use `click_vector_generate.py` to get everyday click vector for each user. 
4. Use `click_3D_1W_1M_generate.py` to get 3-day, 1-week, 1-month click vector.
5. Use `valid_user_grade.py` to output the grade
6. Use `ngram_generate.py` to generate 2, 3, 4, 5 gram!
7. Use `data_db_trans.py` to paste all data together!

(remember to modify the durations for courses data!)

Then you can use the data as you like... in Weka or other machine learning toolkits.

### Others:
Files begin with `daily*` means daily data -- the first n days data. The workflow is the same with normal mode.
