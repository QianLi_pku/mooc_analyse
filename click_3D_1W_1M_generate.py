#!/bin/python
## generate user click sequence for 3-day, 1-week, 1-month
# -- coding: utf-8 --
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# Import lib that can print parsed data
from pprint import pprint
import pymysql.cursors

maxlength = 150 # the longest course is 150 days
fileDir = './output/'
files = ['chemistry-001', 'chemistry-002', 'chemistry-003']
# files = ['bioinfo-001', 'bioinfo-002']
time_sufixes = ['_3D', '_1W', '_1M']
cata_sufixes = ['_forum', '_lecture', '_quiz', '_all']
entries = ['key', 'value', 'username', 'page_url', 'language', 'timestamp', 'user_ip', 'user_agent', 'id', 'session_user_id', 'normal_grade', 'distinction_grade', 'achievement_level', 'authenticated_overall', 'ace_grade', 'passed_ace']
start_times = {'chemistry-001': '2013/9/1', 'chemistry-002': '2014/2/10', 'chemistry-003': '2015/2/1'}
# start_times = {'bioinfo-001': '2013/9/4', 'bioinfo-002': '2014/1/28'}
durations = {'chemistry-001': 121, 'chemistry-002': 126, 'chemistry-003': 149}
# durations = {'chemistry-001': 121, 'chemistry-002': 121, 'chemistry-003': 121}
# durations = {'bioinfo-001': 146, 'bioinfo-002': 88}

for file_name in files:
  duration = durations[file_name]
  for cata_sufix in cata_sufixes:
    input_file = open(fileDir + file_name + '_1D' + cata_sufix + '.txt', 'r')
    users_vector = {}
    for eachLine in input_file:
      words = eachLine.strip('\n').split('\t')
      if len(words) < 1:
        break
      user_id = words[0]
      if user_id not in users_vector:
        users_vector[user_id] = [0 for i in xrange(duration)]
      for i in xrange(1, len(words)):
        users_vector[user_id][i-1] = int(words[i])
      print(users_vector[user_id])
    # The interval between neighbor element
    interval = 1
    for time_sufix in time_sufixes:
      if time_sufix == '_3D':
        interval = 3
      if time_sufix == '_1W':
        interval = 7
      if time_sufix == '_1M':
        interval = 30
      output_file = open(fileDir + file_name + time_sufix + cata_sufix + '.txt', 'w')
      for user in users_vector:
        output_file.write(str(user))
        #print str(user)
        day = 0
        while day < len(users_vector[user]):
          flag = 0
          for i in xrange(day, min(day + interval, len(users_vector[user]))):
            if users_vector[user][i] == 1:
              flag = 1
              break
          output_file.write('\t' + str(flag))
          #print '\t' + str(flag)
          day = day + interval
        output_file.write('\n')
        #print '\n'
      output_file.close()
    input_file.close()
  